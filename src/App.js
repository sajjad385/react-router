import React, {Component} from 'react';
import './App.css';
import  axios from 'axios'
import {BrowserRouter, Route} from 'react-router-dom'


class App extends Component {

    state= {
        posts:[]
    }

    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(response=>{
                this.setState({
                    posts:response.data
                })
            })
    }
    render(){
       let {posts}=this.state

        if (posts.length    === 0){
            return(

                <div className="text-center">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>

            );
        }
        else {
            return(
                <BrowserRouter>
                <div className="App">
                   <Route path='/' render={()=>{
                       return (
                           <h1>Hello world</h1>
                       )
                   }}/>
                </div>
                </BrowserRouter>
            );
        }
    }
}


export default App;
