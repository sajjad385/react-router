import React, {Component} from 'react';
// import logo from './logo.svg';
import './App.css';
// import First from './components/First/First';
// import Second from './components/Second/Second'
// import Third from './components/Third/Third'
// import Counter from './components/Counter/Counter'
// import Example from './components/Example/Example'
import Books from './components/Books/Books'
//

class App extends Component {

    state= {
        books:[
            {
                id: 1,
                name:'JavaScript',
                price:300
            },
            {
                id:2,
                name:'React JS',
                price:400
            },
            {
                id:3,
                name:'React Redux',
                price:350
            },
            {
                id:4,
                name:'Jquery',
                price:200
            },
            {
                id:5,
                name:'Vue js',
                price:240
            },
            {
                id:6,
                name:'Angular',
                price:270
            },
            {
                id:7,
                name:'Laravel',
                price:230
            },
            {
                id:8,
                name:'PHP',
                price:180
            }

        ]
    }

    deleteHandler=(id)=>{
        let newBooks=this.state.books.filter(book => book.id !== id)
        this.setState({
            books:newBooks
        })
    }

    changeHandler= (book,id)=>{
        let newBooks= this.state.books.map(book=>{
            if (id === book.id){
                return{
                    book,
                }
            }
            return book
        })
        this.setState({
            books:newBooks
        })
    }


    render(){
        return(
          <div className="App">
              <h1 style={{margin:'0 auto', textAlign:'center'}}>Event Handler </h1>
              <Books
                  changeHandler={this.changeHandler.bind(this)}
                  deleteHandler={this.deleteHandler.bind(this)}
                  books={this.state.books}/>
          </div>
        );
    }
}


/*

class App extends Component {
    render(){
        let obj= {
            padding: '10px',
            textAlign:'center'
        }
        return(
            <div className="App">
                <h1 style={obj}>Hello Programmers</h1>
            </div>
        );
    }
}
*/








//Stateless component
// const Example=(props)=>{
//     return <h1>Sajjad Hossain</h1>
// }
/*
//Event Handler
class App extends  Component {

    state={
        name:'',
    }
    // clickHandler=(event)=> {
    //     console.log(event.target)
    // }
    inputHandler=(event)=>{
        this.setState({
            name:event.target.value

        })
    }
    render(){
        return(
            <div className="App">
                <div className="container">
                    <input type="text" onChange={this.inputHandler} placeholder="Enter Your name"/>
                    <button onClick={this.clickHandler}>Click me</button>
                    {this.state.name ? <h3>Hello Mr. {this.state.name}</h3>: ''}
                    <Example bismillah='بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ' name="Mohammad Sajjad Hossain" department="Computer Science & Technology" insname="Bangladesh Sweden Polytechnic Institute" address="Kaptai, Rangamati"/>
                </div>
            </div>
        );
    }
}
*/
//Props
// function App() {
//    // let h1 = 'Welcome to React World'
//    // let p = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, alias, delectus deserunt ducimus enim error inventore magni natus nesciunt officiis omnis quisquam quos tempora! Corporis ipsa laboriosam quae quia! Fuga?'
//     return (
//         <div className="App">
//             <First home="Home" about="About Us" service="Portfolio" contact="Contact Us" />
//
//                 <Second/>
//            <Third/>
//             <Counter/>
//         </div>
//     );
// }


/*


 // return React.createElement('div',{className:'App'},[
 //     React.createElement('h1',null, 'Hello World'),
 //     React.createElement('p',null, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, alias, delectus deserunt ducimus enim error inventore magni natus nesciunt officiis omnis quisquam quos tempora! Corporis ipsa laboriosam quae quia! Fuga?')
 // ])
//State
class App extends Component {

    state={
        persons:[
            {home:'Home', about:'About us', service:'My Portfolio', contact:'Contact Me'},
            {home:'My Home ', about:'About Me', service:' Portfolio', contact:'Contact'},
            {home:' your Home ', about:'About Me', service:' Portfolio', contact:'Contact'}
        ]
    }
    render() {
        return (
            <div className="App">
                {this.state.persons.map((people, index)=>{

                    return  <First key={index}
                        home={people.home} about={people.about} service={people.service} contact={people.contact}/>
                })}
                <Second/>
                <Third/>
                <Counter/>
            </div>
        );
    }
}

*/
export default App;
